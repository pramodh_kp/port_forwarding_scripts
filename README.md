## Steps to run the script
- Download the openshift client (openshift-origin-client-tools-v3.9.0-191fece-linux-64bit.tar.gz) from this URL: https://github.com/openshift/origin/releases/tag/v3.9.0
- Copy the the `oc` binary to your PATH. 
- Login to `master.az39.rapyuta.io` and click on `Copy login command` and execute that on your command line. You should be logged into the openshift cluster using the command line
- Execute the script by giving a namespace prefix as an argument. For e.g., ./port_forward.sh qa
- Substitute the URLs with the new URLs printed out by the script.


## FAQ:
### I'm getting some errors saying "Port already in use" while it's already port forwarding.

A: Looks like there are some left over port-forwards from the last time the script was run. Please run `sudo pkill oc`. This will kill all the running `oc port-forward` processes. Re-run the script and it should run properly.
