#!/bin/bash

set -e

declare -A core_services_pod_ports
declare -A core_services_local_ports
declare -A infra_services_pod_ports
declare -A infra_services_local_ports
declare -A other_services_pod_ports
declare -A other_services_local_ports

namespace_prefix=$1

core_services_pod_ports=(["apiserver"]=8080 ["authz"]=8080 ["catalog"]=8600 ["user-docs"]=80)
core_services_local_ports=(["apiserver"]=9080 ["authz"]=9081 ["catalog"]=9082 ["user-docs"]=9083)

infra_services_pod_ports=(["$namespace_prefix-influxdb-influxdb"]=8086 ["elasticsearch"]=9200)
infra_services_local_ports=(["$namespace_prefix-influxdb-influxdb"]=9180 ["elasticsearch"]=9181)

other_services_pod_ports=(["discourse"]=80)
other_services_local_ports=(["discourse"]=9280)

print_config() {
	echo "Point your configurations to the following URLs:"
	echo "apiserver: http://localhost:${core_services_local_ports['apiserver']}"
	echo "dm_endpoint: http://localhost:${core_services_local_ports['apiserver']}/api/device-manager/v0"
	echo "dm_bootstrap_endpoint: http://localhost:${core_services_local_ports['apiserver']}/api/device-manager/v0"
	echo "core_endpoint: http://localhost:${core_services_local_ports['apiserver']}"
	echo "userdocs: http://localhost:${core_services_local_ports['user-docs']}"
	echo "catalog: http://localhost:${core_services_local_ports['catalog']}"
	echo "influx_endpoint: http://localhost:${infra_services_local_ports["$namespace_prefix-influxdb-influxdb"]}"
	echo "elasticsearch_endpoint: http://localhost:${infra_services_local_ports['elasticsearch']}"
	echo "discourse_endpoint: http://localhost:${other_services_local_ports['discourse']}"
	echo "================================================================================="
}

port_forward_core_services() {
	for index in "${!core_services_pod_ports[@]}"
	do
		namespace=$namespace_prefix-rapyuta-core
		pod_port_number=${core_services_pod_ports[$index]}
		local_port_number=${core_services_local_ports[$index]}
		pod_name=$(oc get pods -n $namespace -l app=$index -o custom-columns=':metadata.name' | tail -n 1)
		echo "Port forwarding $index..."
		oc port-forward -n $namespace $pod_name $local_port_number:$pod_port_number &
	done
}

port_forward_infra_services() {
	for index in "${!infra_services_pod_ports[@]}"
	do
		if [ $index = "elasticsearch" ]; then
			namespace=v11-rapyuta-infra
		else
			namespace=$namespace_prefix-rapyuta-infra
		fi
		pod_port_number=${infra_services_pod_ports[$index]}
		local_port_number=${infra_services_local_ports[$index]}
		pod_name=$(oc get pods -n $namespace -l app=$index -o custom-columns=':metadata.name' | tail -n 1)
		echo "Port forwarding $index..."
		oc port-forward -n $namespace $pod_name $local_port_number:$pod_port_number &
	done
}

port_forward_other_services() {
	namespace="discourse"
	pod_port_number=80
	local_port_number=9280
	pod_name=$(oc get pods -n $namespace -l app=web-server -o custom-columns=':metadata.name' | tail -n 1)
	echo "Port forwarding discourse..."
	oc port-forward -n $namespace $pod_name $local_port_number:$pod_port_number &
}

print_config
port_forward_core_services
port_forward_infra_services
port_forward_other_services

wait
